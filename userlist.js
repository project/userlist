
if (Drupal.jsEnabled){
  $(document).ready(function(){
    badges = Drupal.settings.userlist.badges;
    if (badges != null) {
      $("h2[@class='with-tabs']").append('<div class="userlist-profile clearfix">' + badges + '</div>');
    }
  });
}
