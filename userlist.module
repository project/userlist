<?php

/**
 * @file
 * Provides an API to extend the User Adminstration list by adding filters and data columns.
 */

/**
 * Implementation of hook_help().
 */
function userlist_help($section) {
  switch ($section) {
    case 'admin/help#userlist':
      return 
        '<p>' . t('User List provides an API to extend the !user_admin by adding filters and data columns. The module adds one filter and data column through its own API that may help account administration: last access time. Options are provided to hide or reformat several of the default data columns so that the display does not become overly crowded.</p><p>User List displays badge icons to visually indicate commonly needed information for administration: Account blocked, New account, and Account online. These badges may optionally be displayed on user profile pages to user\'s with "administer users" or "view badge icons" access permission.</p><p>User List is packaged with three optional modules to extend its functionality: !User_List_Flag_Account, !User_List_Node_Count and !User_List_TAC_Lite.</p>' .
        '<h3>API</h3><p>Module developers can extend the User Administration user list by including a userlist callback handler in their module.' .
        '<pre>function MODULE_userlist() {
  return array(
    \'KEY\' => array(                  //required: key must be unique

      \'table\' => "TABLENAME",        //required

      \'field\' => "FIELDNAME",        //required

      \'join\' => "FIELD2NAME = 1",    //optional: userlist joins tables on uid
                                     //additional criteria for the join such
                                     //as fid if joining profile_values

      \'aggregate\' => "COUNT",        //optional: aggregate function COUNT, 
                                     //AVG, SUM, MAX, or MIN for field
                                     //userlist will group by u.uid and
                                     //convert any filter WHERE to HAVING

      \'filter\' => array(             //optional: filter is not required, for
                                     //example if only adding data column

        \'title\' => \'FILTER_TITLE\',   //required

        \'operator\' => ">",           //optional: comparison operator
                                     //default is greater than ">"

        \'arg\' => \'%d\',               //required: typically %d or %s

        \'options\' => array(),        //required: key=>value for select

        \'callback\' => \'FUNC_FILTER\', //optional: userlist will callback this
                                     //function with selected key, return
                                     //array of new key value and operator
      ),

      \'data\' => array(               //optional: data is not required, for
                                     //example if only adding new filter

        \'title\' => \'COL_TITLE\',      //required: name of column

        \'callback\' => \'FUNC_DATA\'    //optional: userlist will callback this
                                     //function with $row object which includes
                                     //u.uid, u.name, u.status, u.created, 
                                     //u.access, and any other fields from 
                                     //query, return new value for column
                                     //Note that providing a callback will 
                                     //make data column non-sortable
      )
    )
  );
}</pre>' .
      '<h3>Known Issues</h3><p>User List includes a query builder that helps extend the User Administration user list in most circumstances, but will never be as robust or optimized as a query crafted by hand. For more complex user reporting requirements, another recommended solution would be using Usernode in combination with Views.' . "</p>\n", 
        array(
          '!user_admin' => l(t('User Adminstration list'), 'admin/user/user'),
          '!User_List_Flag_Account' => l(t('User List Flag Account'), 'admin/help/userlist_flag_account'),
          '!User_List_Node_Count' => l(t('User List Node Count'), 'admin/help/userlist_nodecount'),
          '!User_List_TAC_Lite' => l(t('User List TAC Lite'), 'admin/help/userlist_tac_lite')
        ));
    
    case 'admin/settings/userlist':
      return 
        t('User List provides an API to extend the !user_admin by adding filters and data columns. The module adds one filter and data column through its own API that may help account administration: last access time. Options are provided to hide or reformat several of the default data columns so that the display does not become overly crowded. The module also displays badge icons to visually indicate commonly needed information for administration: Account blocked, New account, and Account online.', 
        array(
          '!user_admin' => l(t('User Adminstration list'), 'admin/user/user')
        ));
  }
}

/**
 * Implementation of hook_perm()
 */
function userlist_perm() {
  return array('view badge icons');
}

/**
 * Implementation of hook_menu().
 */
function userlist_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'callback'            => 'drupal_get_form',
      'callback arguments'  => array('userlist_settings'),
      'path'                => 'admin/settings/userlist',
      'title'               => 'User List',
      'description'         => t('Provdes hooks to the User List.'),
      'access'              => user_access('administer site configuration')
    );
    $items[] = array(
      'access'              => user_access('administer site configuration'),
      'callback'            => 'userlist_settings',
      'path'                => 'admin/settings/userlist/userlist',
      'title'               => t('User List'),
      'type'                => MENU_DEFAULT_LOCAL_TASK,
      'weight'              => -10,
    );

    $items[] = array(
      'path' => 'admin/user/user', 
      'title' => t('Users'),
      'description' => t('List, add, and edit users.'),
      'callback' => 'userlist_admin', 
      'callback arguments' => array('list'), 
      'access' => user_access('administer users')
    );
    $items[] = array(
      'path' => 'admin/user/user/list', 
      'title' => t('List'),
      'type' => MENU_DEFAULT_LOCAL_TASK, 
      'weight' => -10
    );
  }
  return $items;
}

/**
 * Menu callback for settings
 *
 * @return
 *   array of form content.
 */
function userlist_settings() {
  $form['default'] = array('#type' => 'fieldset', '#title' => t('Default User List Columns'), '#description' => t('Options for default columns in the User Adminstration list.'));
  $form['default']['userlist_show_status'] = array(
    '#type' => 'select',
    '#title' => t('Show the "Status" column?'),
    '#default_value' => variable_get('userlist_show_status', 1),
    '#options' => array('1' => t('Yes'), '0' => t('No')),
    '#description' => t('The Status column displays account as active or blocked. Blocked status can be shown as an icon badge.')
  );
  $form['default']['userlist_show_member_for'] = array(
    '#type' => 'select',
    '#title' => t('Show the "Member for" column?'),
    '#default_value' => variable_get('userlist_show_member_for', 1),
    '#options' => array('1' => t('Yes'), '0' => t('No')),
    '#description' => t('The Member for column displays how long since the user created their account. New accounts can be shown as an icon badge.')
  );
  $form['default']['userlist_show_operations'] = array(
    '#type' => 'select',
    '#title' => t('Show the "Operations" column?'),
    '#default_value' => variable_get('userlist_show_operations', 1),
    '#options' => array('1' => t('Yes'), '0' => t('No')),
    '#description' => t('The Operations column displays the Edit link.')
  );
  $form['default']['userlist_show_roles'] = array(
    '#type' => 'select',
    '#title' => t('Show the "Roles" column list as:'),
    '#default_value' => variable_get('userlist_show_roles', 1),
    '#options' => array('1' => t('Bullet list'), '0' => t('Comma list')),
    '#description' => t('The Roles column displays as a bullet list which can make rows wrap to new lines if a user has many roles.')
  );

  $form['badge'] = array('#type' => 'fieldset', '#title' => t('User List Badge Icons'));
  $form['badge']['userlist_badge_profile'] = array(
    '#type' => 'select',
    '#title' => t('Show badge icons on user profile page?'),
    '#default_value' => variable_get('userlist_badge_profile', 1),
    '#options' => array('1' => t('Yes'), '0' => t('No'))
  );
  $form['badge']['userlist_badge_new'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#title' => t('Days since account creation to show New account badge'),
    '#default_value' => (int)variable_get('userlist_badge_new', 30),
    '#description' => t('Set to 0 to disable.')
  );
  $form['badge']['userlist_badge_online'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#title' => t('Minutes since last account access to show Account online badge'),
    '#default_value' => (int)variable_get('userlist_badge_online', 15),
    '#description' => t('Set to 0 to disable.')
  );

  $form['userlist_debug_sql'] = array(
    '#type' => 'select',
    '#title' => t('Show query builder debug in status message?'),
    '#default_value' => variable_get('userlist_debug_sql', 0),
    '#options' => array('1' => t('Yes'), '0' => t('No'))
  );
  return system_settings_form($form);
}

function userlist_admin($callback_arg = '') {
  $op = isset($_POST['op']) ? $_POST['op'] : $callback_arg;

  switch ($op) {
    case 'search':
    case t('Search'):
      $output = drupal_get_form('search_form', url('admin/user/search'), $_POST['keys'], 'user') . search_data($_POST['keys'], 'user');
      break;
    case t('Create new account'):
    case 'create':
      $output = drupal_get_form('user_register');
      break;
    default:
      if ($_POST['accounts'] && $_POST['operation'] == 'delete') {
        $output = drupal_get_form('user_multiple_delete_confirm');
      }
      else {
        $output = drupal_get_form('user_filter_form');
        $output .= drupal_get_form('userlist_user_admin_account');
      }
  }
  return $output;
}

/**
 * Implementation of hook_user().
 */
function userlist_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'load':
      $account->userlist_badges = userlist_get_badges($account);
      break;

    case 'view':
      if (arg(0) != 'user' || arg(2)) return;
      if (!variable_get('userlist_badge_profile', 1)) return;
      if (!user_access('administer users') && !user_access('view badge icons')) return;

      drupal_add_js(drupal_get_path('module', 'userlist') . '/userlist.js');
      drupal_add_js(array(
        'userlist' => array(
          'badges' => $account->userlist_badges
        ),
      ), 'setting');

      break;
  }
}

function userlist_form_alter($form_id, &$form) {
  switch ($form_id) {
    case 'user_filter_form':
      userlist_user_filter_form($form_id, $form);
      break;
  }
}

function userlist_user_filter_form($form_id, &$form) {
  global $userlist_callbacks;
  if (!isset($userlist_callbacks)) {
    $userlist_callbacks = module_invoke_all('userlist');
  }
  
  $session = &$_SESSION['user_overview_filter'];
  $session = is_array($session) ? $session : array();
  $filters = user_filters();

  //begin userlist hook
  foreach ($userlist_callbacks as $callback => $value) {
    if (isset($value['filter'])) {
      $filters[$callback] = $value['filter'] + array('table' => $value['table'], 'field' => $value['field']);
    }
  }
  //end userlist hook

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only users where'),
    '#theme' => 'user_filters',
  );
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    $string = ($i++ ? '<em>and</em> where <strong>%a</strong> is <strong>%b</strong>' : '<strong>%a</strong> is <strong>%b</strong>');
    // Merge an array of arrays into one if necessary.
    $options = $type == 'permission' ? call_user_func_array('array_merge', $filters[$type]['options']) : $filters[$type]['options'];
    $form['filters']['current'][] = array('#value' => t($string, array('%a' => $filters[$type]['title'] , '%b' => $options[$value])));
    $filter_active[] = $type;
  }

  foreach ($filters as $key => $filter) {
    // Show filters that are not currently active (in $_SESSION)
    if (!in_array($key, (array)$filter_active)) {
      $names[$key] = $filter['title'];
      $form['filters']['status'][$key] = array(
        '#type' => 'select',
        '#options' => $filter['options'],
      );
    }
  }
  $form['filters']['filter'] = array(
    '#type' => 'radios',
    '#options' => $names,
  );
  $form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => (count($session) ? t('Refine') : t('Filter'))
  );
  if (count($session)) {
    $form['filters']['buttons']['undo'] = array(
      '#type' => 'submit',
      '#value' => t('Undo')
    );
    $form['filters']['buttons']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }

  unset($form['#submit']);
  $form['#submit']['userlist_user_filter_form_submit'] = array($form);
}

function userlist_user_filter_form_submit($form_id, $form_values, $form) {
  global $userlist_callbacks;
  if (!isset($userlist_callbacks)) {
    $userlist_callbacks = module_invoke_all('userlist');
  }
  
  $op = $form_values['op'];
  $filters = user_filters();

  //begin userlist hook
  foreach ($userlist_callbacks as $callback => $value) {
    if (isset($value['filter'])) {
      $filters[$callback] = $value['filter'] + array('table' => $value['table'], 'field' => $value['field'], 'join' => $value['join'], 'aggregate' => $value['aggregate']);
    }
  }
  //end userlist hook

  switch ($op) {
    case t('Filter'): case t('Refine'):
      if (isset($form_values['filter'])) {
        $filter = $form_values['filter'];
        // Merge an array of arrays into one if necessary.
        $options = $filter == 'permission' ? call_user_func_array('array_merge', $filters[$filter]['options']) : $filters[$filter]['options'];
        if (isset($options[$form_values[$filter]])) {
          $_SESSION['user_overview_filter'][] = array($filter, $form_values[$filter]);
        }
      }
      break;
    case t('Undo'):
      array_pop($_SESSION['user_overview_filter']);
      break;
    case t('Reset'):
      $_SESSION['user_overview_filter'] = array();
      break;
    case t('Update'):
      return;
  }

  return 'admin/user/user';
}

function userlist_user_admin_account() {
  global $userlist_callbacks;
  if (!isset($userlist_callbacks)) {
    $userlist_callbacks = module_invoke_all('userlist');
  }
  
  $filter = userlist_build_query();
  $userlist_show_status = variable_get('userlist_show_status', 1);
  $userlist_show_member_for = variable_get('userlist_show_member_for', 1);
  $userlist_show_operations = variable_get('userlist_show_operations', 1);

  $badge_setting['userlist_badge_new'] = variable_get('userlist_badge_new', 30);
  $badge_setting['userlist_badge_online'] = variable_get('userlist_badge_online', 15);

  $badge_setting['add_css'] = true;
  drupal_add_css(drupal_get_path('module', 'userlist') .'/userlist.css');

  if (module_exists('userlist_flag_account')) {
    drupal_add_css(drupal_get_path('module', 'userlist_flag_account') .'/userlist_flag_account.css');
    $badge_setting['userlist_flag_account_flags'] = userlist_flag_account_get_flags();
  }

  $header = array(
    array(), //checkbox
    array(), //badge icons
    array('data' => t('Username'), 'field' => 'u.name')
  );
  if ($userlist_show_status) {
    array_push($header, 
      array('data' => t('Status'), 'field' => 'u.status')
    );
  }
  array_push($header, t('Roles'));

  //begin userlist hook
  foreach ($userlist_callbacks as $callback => $value) {
    if (isset($value['data'])) {
      if ($value['table'] && $value['field'] && !isset($value['data']['callback'])) {
        if ($value['aggregate'] = userlist_get_aggregate_function($value['aggregate'])) {
          $header[] = array('data' => $value['data']['title'], 'field' => $value['field'] . '_' . $value['aggregate']);
        }
        else {
          $header[] = array('data' => $value['data']['title'], 'field' => userlist_get_table_name($value['table']) . '.' . $value['field']);
        }
      }
      else {
        $header[] = $value['data']['title'];
      }
    }
  }
  //end userlist hook
  if ($userlist_show_member_for) {
    array_push($header, 
      array('data' => t('Member for'), 'field' => 'u.created', 'sort' => 'desc'),
      array('data' => t('Last access'), 'field' => 'u.access')
    );
  }
  else {
    array_push($header, 
      array('data' => t('Last access'), 'field' => 'u.access', 'sort' => 'desc')
    );
  }
  if ($userlist_show_operations) {
    array_push($header, 
      t('Operations')
    );
  }

  $sql = 'SELECT DISTINCT ' . $filter['fields'] . ' FROM {users} u LEFT JOIN {users_roles} ur ON (u.uid = ur.uid) ' . $filter['join'] . ' WHERE u.uid != 0 ' . $filter['where'] . $filter['group_by'] . $filter['having'];
  $sql .= tablesort_sql($header);
  if (isset($filter['having_count'])) {
    $query_count = 'SELECT COUNT(*) FROM (SELECT COUNT(DISTINCT u.uid) FROM {users} u LEFT JOIN {users_roles} ur ON (u.uid = ur.uid) ' . $filter['join'] . ' WHERE u.uid != 0 ' . $filter['where'] . $filter['group_by'] . $filter['having_count'] . ') AS subquery';
  }
  else {
    $query_count = 'SELECT COUNT(DISTINCT u.uid) FROM {users} u LEFT JOIN {users_roles} ur ON (u.uid = ur.uid) ' . $filter['join'] . ' WHERE u.uid != 0 ' . $filter['where'];
  }

  $result = pager_query($sql, 50, 0, $query_count, $filter['args']);
  //***DEBUG*** 
  if (variable_get('userlist_debug_sql', 0)) {
    drupal_set_message("<strong>userlist sql=</strong>" . $sql . " (<strong>args=</strong>". implode("|", $filter['args']) . ")", 'warning');
    drupal_set_message("<strong>userlist sql pager=</strong>" . $query_count . " (<strong>args=</strong>". implode("|", $filter['args']) . ")", 'warning');
  }

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $options = array();
  foreach (module_invoke_all('user_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'unblock',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  $form['#theme'] = 'userlist_user_admin_account';

  $destination = drupal_get_destination();

  $status = array(t('blocked'), t('active'));
  $roles = user_roles(1);

  while ($account = db_fetch_object($result)) {
    $accounts[$account->uid] = '';
    $form['badges'][$account->uid] = array('#value' => userlist_get_badges($account, $badge_setting));
    $form['name'][$account->uid] = array('#value' => theme('username', $account));
    if ($userlist_show_status) {
      $form['status'][$account->uid] =  array('#value' => $status[$account->status]);
    }

    $users_roles = array();
    $roles_result = db_query('SELECT rid FROM {users_roles} WHERE uid = %d', $account->uid);
    while ($user_role = db_fetch_object($roles_result)) {
      $users_roles[] = $roles[$user_role->rid];
    }
    asort($users_roles);

    if (variable_get('userlist_show_roles', 1)) {
      $form['roles'][$account->uid][0] = array('#value' => theme('item_list', $users_roles)); 
    }
    else {
      $form['roles'][$account->uid][0] = array('#value' => implode(', ', $users_roles));
    }

    //begin userlist hook
    foreach ($userlist_callbacks as $callback => $value) {
      if (isset($value['data'])) {
        if (function_exists($value['data']['callback'])) {
          $form[$callback][$account->uid] = array('#value' => call_user_func($value['data']['callback'], $account)); //pass row object - not full account
        }
        else if ($value['field']) {
          if ($value['aggregate'] = userlist_get_aggregate_function($value['aggregate'])) {
            $value['field'] = $value['field'] . '_' . $value['aggregate'];
          }
          $form[$callback][$account->uid] = array('#value' => $account->$value['field']);
        }
      }
    }
    //end userlist hook

    if ($userlist_show_member_for) {
      $form['member_for'][$account->uid] = array('#value' => format_interval(time() - $account->created));
    }
    $form['last_access'][$account->uid] =  array('#value' => $account->access ? format_interval(time() - $account->access) : t('never'));
    if ($userlist_show_operations) {
      $form['operations'][$account->uid] = array('#value' => l(t('edit'), "user/$account->uid/edit", array(), $destination));
    }
  }
  $form['accounts'] = array(
    '#type' => 'checkboxes',
    '#options' => $accounts
  );
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  return $form;
}

function theme_userlist_user_admin_account($form) {
  global $userlist_callbacks;
  if (!isset($userlist_callbacks)) {
    $userlist_callbacks = module_invoke_all('userlist');
  }
  
  // Overview table:
  $userlist_show_status = variable_get('userlist_show_status', 1);
  $userlist_show_member_for = variable_get('userlist_show_member_for', 1);
  $userlist_show_operations = variable_get('userlist_show_operations', 1);

  $header = array(
    theme('table_select_header_cell'),
    array('data' => '&nbsp;', 'class' => 'userlist-badges'), //badge icons
    array('data' => t('Username'), 'class' => 'userlist-nowrap', 'field' => 'u.name')
  );
  if ($userlist_show_status) {
    array_push($header, 
      array('data' => t('Status'), 'class' => 'userlist-nowrap', 'field' => 'u.status')
    );
  }
  array_push($header, t('Roles'));

  //begin userlist hook
  foreach ($userlist_callbacks as $callback => $value) {
    if (isset($value['data'])) {
      if ($value['table'] && $value['field'] && !isset($value['data']['callback'])) {
        $header[] = array('data' => $value['data']['title'], 'class' => 'userlist-nowrap', 'field' => userlist_get_table_name($value['table']) . '.' . $value['field']);
      }
      else {
        $header[] = array('data' => $value['data']['title'], 'class' => 'userlist-nowrap');
      }
      $colspan++;
    }
  }
  //end userlist hook

  if ($userlist_show_member_for) {
    array_push($header, 
      array('data' => t('Member for'), 'class' => 'userlist-nowrap', 'field' => 'u.created', 'sort' => 'desc'),
      array('data' => t('Last access'), 'class' => 'userlist-nowrap', 'field' => 'u.access')
    );
  }
  else {
    array_push($header, 
      array('data' => t('Last access'), 'class' => 'userlist-nowrap', 'field' => 'u.access', 'sort' => 'desc')
    );
  }
  if ($userlist_show_operations) {
    array_push($header, 
      t('Operations')
    );
  }

  $output = drupal_render($form['options']);
  if (isset($form['name']) && is_array($form['name'])) {
    foreach (element_children($form['name']) as $key) {
      $rows[$i] = array(
        drupal_render($form['accounts'][$key]),
        array(
          'data' => drupal_render($form['badges'][$key]),
          'class' => 'userlist-nowrap'
        ),
        drupal_render($form['name'][$key])
      );
      if ($userlist_show_status) {
        array_push($rows[$i], 
          drupal_render($form['status'][$key])
        );
      }
      array_push($rows[$i], 
		    drupal_render($form['roles'][$key])
      );

      //begin userlist hook
      foreach ($userlist_callbacks as $callback => $value) {
        if (isset($value['data'])) {
          array_push($rows[$i], drupal_render($form[$callback][$key]));
        }
      }
      //end userlist hook
      if ($userlist_show_member_for) {
        array_push($rows[$i],
          array(
            'data' => drupal_render($form['member_for'][$key]),
            'class' => 'userlist-nowrap'
          )
        );
      }
      array_push($rows[$i],
          array(
            'data' => drupal_render($form['last_access'][$key]),
            'class' => 'userlist-nowrap'
          )
      );
      if ($userlist_show_operations) {
        array_push($rows[$i],
          drupal_render($form['operations'][$key])
        );
      }

      $i++;
    }
  }
  else  {
    if ($userlist_show_status) {
      $optional_columns++;
    }
    if ($userlist_show_member_for) {
      $optional_columns++;
    }
    if ($userlist_show_member_for) {
      $optional_columns++;
    }
    if ($userlist_show_operations) {
      $optional_columns++;
    }
    $rows[] = array(array('data' => t('No users available.'), 'colspan' => 5 + $optional_columns + $colspan));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

/**
 * Submit the user administration update form.
 */
function userlist_user_admin_account_submit($form_id, $form_values) {
  //pass to user module
  user_admin_account_submit($form_id, $form_values);
}

/**
 * Build query.
 */
function userlist_build_query() {
  global $userlist_callbacks;
  if (!isset($userlist_callbacks)) {
    $userlist_callbacks = module_invoke_all('userlist');
  }
  
  $filters = user_filters();
  $where = $args = $join = array();
  $fields = array('u.uid', 'u.name', 'u.status', 'u.created', 'u.access');

  //begin userlist hook
  foreach ($userlist_callbacks as $callback => $value) {
    if (isset($value['filter'])) {
      $filters[$callback] = $value['filter'] + array('table' => $value['table'], 'field' => $value['field'], 'join' => $value['join'], 'aggregate' => $value['aggregate']);
    }

    //==table
    if ($table = userlist_get_table_name($value['table'])) {
      if (!in_array($table, array('u', 'ur'))) { //join if not users or users_roles
        $join[] = $value['join'] ? 
          'LEFT JOIN {' . $table . '} ' . $table . ' ON (u.uid = ' . $table . '.uid' . ' AND ' . $table . '.' . $value['join'] .  ')' :
          'LEFT JOIN {' . $table . '} ' . $table . ' ON (u.uid = ' . $table . '.uid' . ')';
      }
    }

    //==fields
    if ($table && $value['field']) {
      if ($value['aggregate'] = userlist_get_aggregate_function($value['aggregate'])) {
        $fields[] = strtoupper($value['aggregate']) . '(DISTINCT ' . $table . '.' . $value['field'] . ') AS ' . $value['field'] . '_' . $value['aggregate'];
        $group_by = ' GROUP BY u.uid';
      }
      else {
        $fields[] = $table . '.' . $value['field'];
      }
    }
  }
  //end userlist hook

  // Build query
  foreach ($_SESSION['user_overview_filter'] as $filter) {
    list($key, $value) = $filter;
    // This checks to see if this permission filter is an enabled permission for the authenticated role.
    // If so, then all users would be listed, and we can skip adding it to the filter query.
    if ($key == 'permission') {
      $account = new stdClass();
      $account->uid = 'user_filter';
      $account->roles = array(DRUPAL_AUTHENTICATED_RID => 1);
      if (user_access($value, $account)) {
        continue;
      }
    }

    //begin userlist hook
    //==args
    if ($filters[$key]['callback'] && function_exists($filters[$key]['callback'])) {
      list($args[],$operator) = call_user_func($filters[$key]['callback'], $value);
    }
    else {
      $args[] = $value;
    }

    //==where
    if ($filters[$key]['table'] && $filters[$key]['field'] && $filters[$key]['arg']) {
      $table = userlist_get_table_name($filters[$key]['table']);
      $field = $filters[$key]['field'];
      if (!$operator) {
        $operator = $filters[$key]['operator'] ? $filters[$key]['operator'] : ">";
      }
      if ($filters[$key]['aggregate'] = userlist_get_aggregate_function($filters[$key]['aggregate'])) {
        // format: field_count > %d
        $having[] = $field . '_' . $filters[$key]['aggregate'] . " " . $operator . " " . $filters[$key]['arg'];
        // format: COUNT(DISTINCT table.field) > %d
        $having_count[] = strtoupper($filters[$key]['aggregate']) . '(DISTINCT ' . $table . '.' . $field . ') ' . $operator . " " . $filters[$key]['arg'];
        //pop from args
        $args_having[] = array_pop($args);
      }
      else {
        // format: table.field > %d
        $where[] = $table . '.' . $field . " " . $operator . " " . $filters[$key]['arg'];
      }

    }
    elseif ($filters[$key]['where']) {
      $where[] = $filters[$key]['where'];
    }
    //end userlist hook

    unset($operator);
  }

  $fields = implode(', ', array_unique($fields));
  $join = count($join) ? ' ' .implode(' ', array_unique($join)) : '';

  $where = count($where) ? 'AND ' .implode(' AND ', $where) : '';
  $args = array_merge($args, (array)$args_having);

  if (count($having)) $having = ' HAVING ' . implode(' AND ', $having);
  if (count($having_count)) $having_count = ' HAVING ' . implode(' AND ', $having_count);

  return array(
    'fields' => $fields,
    'join' => $join,
    'where' => $where,
    'args' => $args,
    'having' => $having,
    'having_count' => $having_count,
    'group_by' => $group_by,
  );
}

/**
 * Build query helper.
 */
function userlist_get_table_name($table) {
  if ($table == 'users') return 'u';
  if ($table == 'users_roles') return 'ur';
  return db_escape_table($table);
}

/**
 * Build query helper.
 */
function userlist_get_aggregate_function($function) {
  $function = strtolower($function);
  if (in_array($function, array('count', 'avg', 'sum', 'max', 'min'))) {
    return $function;
  }
  return FALSE;
}

/**
 * Internal function.
 */
function userlist_get_badges($account, $param = array()) {
  // $param can be passed so that we don't have to rebuild for every account on user list
  if (!isset($param['userlist_badge_new']))     $param['userlist_badge_new'] = variable_get('userlist_badge_new', 30);
  if (!isset($param['userlist_badge_online']))  $param['userlist_badge_online'] = variable_get('userlist_badge_online', 15);

  $time = time();
  $new_time = $time - 3600*24*$param['userlist_badge_new'];  //30 days
  $online_time = $time - 60*$param['userlist_badge_online']; //15 minutes
  $badges = '';

  if (!$param['add_css']) drupal_add_css(drupal_get_path('module', 'userlist') . '/userlist.css');

  if ($param['userlist_badge_online'] && $account->access && $account->access > $online_time) {
    $badges .= '<span class="userlist-badge userlist-account-online" title="' . t('Account online') . '">online</span>';
  }
  if ($param['userlist_badge_new'] && $account->created > $new_time) {
    $badges .= '<span class="userlist-badge userlist-account-new" title="' . t('New account') . '">new</span>';
  }
  if ($account->status == 0) {
    $badges .= '<span class="userlist-badge userlist-account-blocked" title="' . t('Account blocked') . '">blocked</span>';
  }

  if (module_exists('userlist_flag_account')) {
    if (!isset($param['userlist_flag_account_flags'])) $param['userlist_flag_account_flags'] = userlist_flag_account_get_flags();
    if (!$param['add_css']) drupal_add_css(drupal_get_path('module', 'userlist_flag_account') . '/userlist_flag_account.css');
    $badges .= userlist_flag_account_get_badges($param['userlist_flag_account_flags'], $account->uid);
  }

  return $badges;
}

/**
 * Callback handler for userlist
 */
function userlist_userlist() {
  return array(
    'last_access' => array(
      'table' => "users",
      'field' => "access",
      'filter' => array(
        'title' => t('last access'),
        'operator' => ">",
        'arg' => '%d',
        'options' => array(0 => 'never') + drupal_map_assoc(array(300, 900, 3600, 86400, 604800, 2419200, 7257600, 31536000), 'format_interval'),
        'callback' => 'userlist_last_access_filter',
      )
    )
  );
}

function userlist_last_access_filter($value) {
  if ($value == 0) {
    return array(0, "=");
  }
  return array(time() - $value);
}
