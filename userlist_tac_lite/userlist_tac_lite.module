<?php

/**
 * @file
 * Display Taxonomy Access Control Lite terms in User List.
 */

/**
 * Implementation of hook_help().
 */
function userlist_tac_lite_help($section) {
  switch ($section) {
    case 'admin/help#userlist_tac_lite':
      return 
        '<p>' . t('This module extends the !User_List module and enables Taxonomy Access Control Lite terms to be displayed and filtered from the !user_admin. The module currently only supports one vocabulary for !tac.' . "</p>\n", 
        array(
          '!User_List' => l(t('User List'), 'admin/help/userlist'),
          '!user_admin' => l(t('User Adminstration list'), 'admin/user/user'),
          '!tac' => l(t('access control by taxonomy'), 'admin/user/access/tac_lite')
        ));
  }
}

/**
 * Callback handler for userlist.
 */
function userlist_tac_lite_userlist() {
  $vids = variable_get('tac_lite_categories', null);
  if (count($vids) != 1) {
    drupal_set_message(t('Please select only one vocabulary for !tac.', array('!tac' => l(t('access control by taxonomy'), 'admin/user/access/tac_lite'))), 'warning');
    return;
  }

  //flatten array and get vocab name
  $vid = array_shift(array_keys($vids));
  $vocab = taxonomy_get_vocabulary($vid);

  // set first option to "any"
  $options = array('%"tac_lite";%' => 'any');
  
  // we need to grab term data for tac_lite 'user access'
  // and put into an array so it can be selected from pulldown menu
  // note that the format needs to follow tac_lite serialized user data
  $sql = "SELECT td.vid, td.tid, td.name FROM {term_data} td 
            LEFT JOIN {vocabulary} v ON td.vid = v.vid 
              WHERE v.vid = %d 
                ORDER BY td.name";
  $result = db_query($sql, $vid);
  while ($data = db_fetch_object($result)) {
    $options['%"tac_lite";a:1:{i:'.$data->vid.';a:1:{i:'.$data->tid.';%'] = $data->name;
  }

  return array(
    'userlist_tac_lite' => array(
      'table' => "users",
      'field' => "data",
      'filter' => array(
        'title' => strtolower($vocab->name),
        'operator' => "LIKE",
        'arg' => '"%s"',
        'options' => $options,
      ),
      'data' => array(
        'title' => $vocab->name,
        'callback' => 'userlist_tac_lite_data',
      )
    )
  );
}

function userlist_tac_lite_data($row) {
  if ($account = user_load(array('uid' => $row->uid))) {
    $vids = variable_get('tac_lite_categories', null);
	  if (count($vids)) {
      foreach ($vids as $vid) {
        // this value is not necessarily set for a given $user object
        if (is_array($account->tac_lite)) {
          foreach ($account->tac_lite[$vid] as $tid) {
            $output[] = taxonomy_get_term($tid)->name;
          }
        }
      }
	  }

    return implode(', ', (array)$output);
  }
  return;
}
