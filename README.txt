

Description
-------------
User List provides an API to extend the User Administration user list 
(admin/user/user) by adding filters and data columns. The module adds one 
filter and data column through its own API that may help account 
administration: last access time. Options are provided to hide or reformat 
several of the default data columns so that the display does not become overly 
crowded.

User List displays badge icons to visually indicate commonly needed information 
for administration: Account blocked, New account, and Account online. These 
badges may optionally be displayed on user profile pages to user's with 
"administer users" or "view badge icons" access permission.

User List is packaged with three optional modules to extend its functionality. 
  User List Flag Account: accounts can be flagged and filtered with badge icons
  User List Node Count: number of nodes created by users displayed and filtered
  User List TAC Lite: Taxonomy Access Control Lite terms displayed and filtered


Prerequisites
-------------
None


Installation
-------------
1. copy the userlist directory and all its contents to your 
   modules directory.
2. enable the module: admin/build/modules.
3. configure the module: admin/user/settings/userlist
4. (optional) grant roles "view badge icons" access: admin/user/user/access


API
-------------
Module developers can extend the User Administration user list by including a
userlist callback handler in their module.

function MODULE_userlist() {
  return array(
    'KEY' => array(                  //required: key must be unique

      'table' => "TABLENAME",        //required

      'field' => "FIELDNAME",        //required

      'join' => "FIELD2NAME = 1",    //optional: userlist joins tables on uid
                                     //additional criteria for the join such
                                     //as fid if joining profile_values

      'aggregate' => "COUNT",        //optional: aggregate function COUNT, 
                                     //AVG, SUM, MAX, or MIN for field
                                     //userlist will group by u.uid and
                                     //convert any filter WHERE to HAVING

      'filter' => array(             //optional: filter is not required, for
                                     //example if only adding data column

        'title' => 'FILTER_TITLE',   //required

        'operator' => ">",           //optional: comparison operator
                                     //default is greater than ">"

        'arg' => '%d',               //required: typically %d or %s

        'options' => array(),        //required: key=>value for select

        'callback' => 'FUNC_FILTER', //optional: userlist will callback this
                                     //function with selected key, return
                                     //array of new key value and operator
      ),

      'data' => array(               //optional: data is not required, for
                                     //example if only adding new filter

        'title' => 'COL_TITLE',      //required: name of column

        'callback' => 'FUNC_DATA'    //optional: userlist will callback this
                                     //function with $row object which includes
                                     //u.uid, u.name, u.status, u.created, 
                                     //u.access, and any other fields from 
                                     //query, return new value for column
                                     //Note that providing a callback will 
                                     //make data column non-sortable
      )
    )
  );
}



Known Issues
-------------
User List includes a query builder that helps extend the User Administration 
user list in most circumstances, but will never be as robust or optimized as a
query crafted by hand. For more complex user reporting requirements, another 
recommended solution would be using Usernode in combination with Views.


Bugs/Features/Patches
---------------------
If you want to report bugs, feature requests, or submit a patch, please do so 
at the project page on the Drupal web site.
http://drupal.org/project/userlist


Todo List
-------------
+ add support for field aliases
+ add support for multiple joins to the same table
+ extend API to badge icons


Credits
-------------
Module development sponsored by LifeWire, a subsidiary of The New York Times 
Company.
http://www.lifewire.com

Icons were created by Mark James and licensed under a Creative Commons 
Attribution 2.5 License. 
http://famfamfam.com


Author
-------------
John Money
ossemble LLC.
http://ossemble.com
